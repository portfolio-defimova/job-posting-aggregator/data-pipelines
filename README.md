# hello_airflow

## How to

### Setup for local development

1. All commands should be run in Pipenv shell:

`pipenv shell`

2. Install Airflow library to your venv to resolve imports:

`pip install "apache-airflow[celery]==2.8.1" --constraint "https://raw.githubusercontent.com/apache/airflow/constraints-2.8.1/constraints-3.11.txt"`

https://airflow.apache.org/docs/apache-airflow/stable/installation/installing-from-pypi.html

3. Run local DWH in docker:

`docker-compose -f docker-compose-dwh.yml up -d`

4. Run Airflow in Docker for local development:

https://airflow.apache.org/docs/apache-airflow/stable/howto/docker-compose/index.html

(skip) Get docker-compose.yaml:
https://airflow.apache.org/docs/apache-airflow/2.8.1/docker-compose.yaml

(skip) `mkdir -p ./dags ./logs ./plugins ./config`

Set Airflow user:

`echo -e "AIRFLOW_UID=$(id -u)" > .env`

Initialize Airflow backend DB:

`docker compose up airflow-init`

Start webserver, scheduler, etc.:

`docker compose up -d`

5. To run CLI commands like `airflow dags list`, use one of the defined airflow-* services:

`docker exec hello_airflow-airflow-triggerer-1 airflow dags list`
